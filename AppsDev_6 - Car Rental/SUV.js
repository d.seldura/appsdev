"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SUV = /** @class */ (function () {
    function SUV(input) {
        this.suvCollection = [];
        this.suvCollection = input;
    }
    SUV.prototype.getAllofType = function () {
        var suvList = [];
        for (var _i = 0, _a = this.suvCollection; _i < _a.length; _i++) {
            var index = _a[_i];
            if (index.type == "suv") {
                suvList.push(index);
            }
        }
        return suvList;
    };
    return SUV;
}());
exports.SUV = SUV;
