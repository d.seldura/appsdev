import { Vehicle } from './Vehicle';

export class Sedan {
      protected sedanCollection: Array<Vehicle> = [];

      constructor(input: Array<Vehicle>){
             this.sedanCollection = input; 
      }

     getAllofType(): Array<Vehicle>{
           let sedanList: Array<Vehicle> = [];
           for(let index of this.sedanCollection){
               if(index.type=="sedan"){
                   sedanList.push(index);
               }
           }
           return sedanList;
     } 
}