import { Vehicle } from './Vehicle';

export class Pickup {
      protected pickupCollection: Array<Vehicle> = [];

      constructor(input: Array<Vehicle>){
             this.pickupCollection = input; 
      }

     getAllofType(): Array<Vehicle>{
           let pickupList: Array<Vehicle> = [];
           for(let index of this.pickupCollection){
               if(index.type=="pickup"){
                   pickupList.push(index);
               }
           }
           return pickupList;
     } 
}