"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Motorcycle = /** @class */ (function () {
    function Motorcycle(input) {
        this.motorcycleCollection = [];
        this.motorcycleCollection = input;
    }
    Motorcycle.prototype.getAllofType = function () {
        var motorcycleList = [];
        for (var _i = 0, _a = this.motorcycleCollection; _i < _a.length; _i++) {
            var index = _a[_i];
            if (index.type == "motorcycle") {
                motorcycleList.push(index);
            }
        }
        return motorcycleList;
    };
    return Motorcycle;
}());
exports.Motorcycle = Motorcycle;
