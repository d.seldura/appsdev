import { Vehicle } from './Vehicle';

export class Van {
      protected vanCollection: Array<Vehicle> = [];

      constructor(input: Array<Vehicle>){
             this.vanCollection = input; 
      }

     getAllofType(): Array<Vehicle>{
           let vanList: Array<Vehicle> = [];
           for(let index of this.vanCollection){
               if(index.type=="van"){
                   vanList.push(index);
               }
           }
           return vanList;
     } 
}