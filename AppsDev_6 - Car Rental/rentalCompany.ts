import { Motorcycle } from './Motorcycle';
import { Minibus } from './Minibus';
import { Pickup } from './Pickup';
import { Sedan } from './Sedan';
import { SUV } from './SUV';
import { Van } from './Van';
import { VehicleList } from './VehicleList';
import { ObjectDisplay } from './ObjectDisplay';
import { Order } from './Order';


let vehiclesRental = new VehicleList();

let motorcycleRental = new Motorcycle(vehiclesRental.getAllVehicles());

//7
let pickupRental = new Pickup(vehiclesRental.getAllVehicles());
//8
let sedanRental = new Sedan(vehiclesRental.getAllVehicles());
//9
let suvRental = new SUV(vehiclesRental.getAllVehicles());
//10
let vanRental = new Van(vehiclesRental.getAllVehicles());

let minibusRental = new Minibus(vehiclesRental.getAllVehicles());

let showOptions = new ObjectDisplay("vehicleOptions");

let buttons = document.getElementsByTagName("button");
let fourWheel = document.getElementById("four_wheel_options") as HTMLElement;

class rentalCompany {
    constructor() {
        for (let i =0; i<buttons.length; i++){
        buttons[i].addEventListener("click", (e: Event) => this.buttonEvent(Number(buttons[i].value)));
        //Debug button.value
        //console.log(buttons[i].value);
        }
    }

    buttonEvent(n: number) {
        console.log("DEBUG - B/E" + n);
        showOptions.cleanSlate();
        switch(n){
            case 20:
                fourWheel.style.display = "none";
                for (let item of motorcycleRental.getAllofType()){
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 40:
                fourWheel.style.display = "block";
                break;
            case 60:
                fourWheel.style.display = "none";
                for (let item of minibusRental.getAllofType()){
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 7:
                for (let item of pickupRental.getAllofType()){
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 8:
                for (let item of sedanRental.getAllofType()){
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 9:
                    for (let item of suvRental.getAllofType()){
                        showOptions.addToStorage(item);
                        //console.log(item.brand+item.model);
                    }
                    break;
            case 10:
                for (let item of vanRental.getAllofType()){
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
    }
        showOptions.addToDisplay();
        showOptions.display();
        let rentalOrder = new Order();
        rentalOrder.hideReceipt();
    }
}


let rentARide = new rentalCompany();

