"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Pickup = /** @class */ (function () {
    function Pickup(input) {
        this.pickupCollection = [];
        this.pickupCollection = input;
    }
    Pickup.prototype.getAllofType = function () {
        var pickupList = [];
        for (var _i = 0, _a = this.pickupCollection; _i < _a.length; _i++) {
            var index = _a[_i];
            if (index.type == "pickup") {
                pickupList.push(index);
            }
        }
        return pickupList;
    };
    return Pickup;
}());
exports.Pickup = Pickup;
