"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Sedan = /** @class */ (function () {
    function Sedan(input) {
        this.sedanCollection = [];
        this.sedanCollection = input;
    }
    Sedan.prototype.getAllofType = function () {
        var sedanList = [];
        for (var _i = 0, _a = this.sedanCollection; _i < _a.length; _i++) {
            var index = _a[_i];
            if (index.type == "sedan") {
                sedanList.push(index);
            }
        }
        return sedanList;
    };
    return Sedan;
}());
exports.Sedan = Sedan;
