"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Van = /** @class */ (function () {
    function Van(input) {
        this.vanCollection = [];
        this.vanCollection = input;
    }
    Van.prototype.getAllofType = function () {
        var vanList = [];
        for (var _i = 0, _a = this.vanCollection; _i < _a.length; _i++) {
            var index = _a[_i];
            if (index.type == "van") {
                vanList.push(index);
            }
        }
        return vanList;
    };
    return Van;
}());
exports.Van = Van;
