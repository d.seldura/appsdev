import { Vehicle } from './Vehicle';

export class Motorcycle {
      protected motorcycleCollection: Array<Vehicle> = [];

      constructor(input: Array<Vehicle>){
             this.motorcycleCollection = input; 
      }

     getAllofType(): Array<Vehicle>{
           let motorcycleList: Array<Vehicle> = [];
           for(let index of this.motorcycleCollection){
               if(index.type=="motorcycle"){
                   motorcycleList.push(index);
               }
           }
           return motorcycleList;
     } 
}