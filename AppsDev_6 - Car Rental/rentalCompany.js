"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Motorcycle_1 = require("./Motorcycle");
var Minibus_1 = require("./Minibus");
var Pickup_1 = require("./Pickup");
var Sedan_1 = require("./Sedan");
var SUV_1 = require("./SUV");
var Van_1 = require("./Van");
var VehicleList_1 = require("./VehicleList");
var ObjectDisplay_1 = require("./ObjectDisplay");
var Order_1 = require("./Order");
var vehiclesRental = new VehicleList_1.VehicleList();
var motorcycleRental = new Motorcycle_1.Motorcycle(vehiclesRental.getAllVehicles());
//7
var pickupRental = new Pickup_1.Pickup(vehiclesRental.getAllVehicles());
//8
var sedanRental = new Sedan_1.Sedan(vehiclesRental.getAllVehicles());
//9
var suvRental = new SUV_1.SUV(vehiclesRental.getAllVehicles());
//10
var vanRental = new Van_1.Van(vehiclesRental.getAllVehicles());
var minibusRental = new Minibus_1.Minibus(vehiclesRental.getAllVehicles());
var showOptions = new ObjectDisplay_1.ObjectDisplay("vehicleOptions");
var buttons = document.getElementsByTagName("button");
var fourWheel = document.getElementById("four_wheel_options");
var rentalCompany = /** @class */ (function () {
    function rentalCompany() {
        var _this = this;
        var _loop_1 = function (i) {
            buttons[i].addEventListener("click", function (e) { return _this.buttonEvent(Number(buttons[i].value)); });
        };
        for (var i = 0; i < buttons.length; i++) {
            _loop_1(i);
        }
    }
    rentalCompany.prototype.buttonEvent = function (n) {
        console.log("DEBUG - B/E" + n);
        showOptions.cleanSlate();
        switch (n) {
            case 20:
                fourWheel.style.display = "none";
                for (var _i = 0, _a = motorcycleRental.getAllofType(); _i < _a.length; _i++) {
                    var item = _a[_i];
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 40:
                fourWheel.style.display = "block";
                break;
            case 60:
                fourWheel.style.display = "none";
                for (var _b = 0, _c = minibusRental.getAllofType(); _b < _c.length; _b++) {
                    var item = _c[_b];
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 7:
                for (var _d = 0, _e = pickupRental.getAllofType(); _d < _e.length; _d++) {
                    var item = _e[_d];
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 8:
                for (var _f = 0, _g = sedanRental.getAllofType(); _f < _g.length; _f++) {
                    var item = _g[_f];
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 9:
                for (var _h = 0, _j = suvRental.getAllofType(); _h < _j.length; _h++) {
                    var item = _j[_h];
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 10:
                for (var _k = 0, _l = vanRental.getAllofType(); _k < _l.length; _k++) {
                    var item = _l[_k];
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
        }
        showOptions.addToDisplay();
        showOptions.display();
        var rentalOrder = new Order_1.Order();
        rentalOrder.hideReceipt();
    };
    return rentalCompany;
}());
var rentARide = new rentalCompany();
