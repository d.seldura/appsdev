"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ObjectDisplay = /** @class */ (function () {
    function ObjectDisplay(input) {
        this.target = document.getElementById(input);
        this.storage = [];
        this.parsedData = "";
        this.currentID = 0;
    }
    ObjectDisplay.prototype.addToStorage = function (input) {
        this.storage.push(input);
    };
    ObjectDisplay.prototype.addToDisplay = function () {
        var update = "";
        for (var _i = 0, _a = this.storage; _i < _a.length; _i++) {
            var index = _a[_i];
            update += "<div class=\"squareish\" id=\"option_" + this.currentID + "\"><h4>" + index.brand + " " + index.model + "</h4><p>Php." + index.pricePerDay + "/day</p>" + "<button id=\"RentOption_" + this.currentID + "\" value=\"" + this.currentID + "\">RENT</button></div>";
            console.log(update);
            this.currentID += 1;
            console.log("currentID " + this.currentID);
        }
        this.parsedData = update;
    };
    ObjectDisplay.prototype.cleanSlate = function () {
        this.storage = [];
        this.parsedData = "";
        this.currentID = 0;
        this.display();
    };
    ObjectDisplay.prototype.display = function () {
        this.target.innerHTML = this.parsedData;
    };
    return ObjectDisplay;
}());
exports.ObjectDisplay = ObjectDisplay;
