"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rentalChoices = document.getElementsByTagName("button");
var receiptVisibility = document.getElementById("receipt");
var Order = /** @class */ (function () {
    function Order() {
        var _this = this;
        var _loop_1 = function (i) {
            rentalChoices[i].addEventListener("click", function (e) { return _this.buttonEvent(Number(rentalChoices[i].value)); });
            console.log("RentalChoices" + rentalChoices[i].value);
        };
        for (var i = 0; i < rentalChoices.length; i++) {
            _loop_1(i);
        }
    }
    Order.prototype.buttonEvent = function (n) {
        //console.log(n);
        receiptVisibility.style.display = "block";
        var selectedOption = document.getElementById("option_" + n);
        var receiptData = selectedOption.innerText.split("\n");
        //console.log(receiptData);
        var userNameInput = document.getElementById("userName");
        var numberInput = document.getElementById("phoneNumber");
        var daysInput = document.getElementById("days");
        document.getElementById("vehicle").innerHTML = receiptData[0];
        document.getElementById("user").innerHTML = userNameInput.value == "" ? "Name Unavailable" : userNameInput.value;
        document.getElementById("number").innerHTML = numberInput.value == "" ? "Number Unavailable" : numberInput.value;
        document.getElementById("cost").innerHTML = receiptData[2];
        document.getElementById("daysToRent").innerHTML = daysInput.value == "" ? "1" : daysInput.value;
        var costStr = receiptData[2].match(/(\d+)/);
        if (numberInput.value == "")
            document.getElementById("balance").innerHTML = 1 * Number(costStr[0]);
        else
            document.getElementById("balance").innerHTML = Number(daysInput.value) * Number(costStr[0]);
    };
    Order.prototype.hideReceipt = function () {
        receiptVisibility.style.display = "none";
    };
    return Order;
}());
exports.Order = Order;
