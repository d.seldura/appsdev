import { Vehicle } from './Vehicle';

export class SUV {
      protected suvCollection: Array<Vehicle> = [];

      constructor(input: Array<Vehicle>){
             this.suvCollection = input; 
      }

     getAllofType(): Array<Vehicle>{
           let suvList: Array<Vehicle> = [];
           for(let index of this.suvCollection){
               if(index.type=="suv"){
                   suvList.push(index);
               }
           }
           return suvList;
     } 
}