import { Vehicle } from './Vehicle';

export class VehicleList {
     private vehicleCollection: Array<Vehicle> = [];

     constructor(){
         this.vehicleCollection = [
             //motorcycle
            {noOfWheels:2,model:'Mio',brand:'Yamaha',type:'motorcycle',pricePerDay: 448},
            {noOfWheels:2,model:'Scoopy',brand:'Honda',type:'motorcycle',pricePerDay: 350},
            {noOfWheels:2,model:'Beat 110cc',brand:'Honda',type:'motorcycle',pricePerDay: 448},
            {noOfWheels:2,model:'Nmax 155cc',brand:'Yamaha',type:'motorcycle',pricePerDay: 599},
            {noOfWheels:2,model:'Cafe Racer 152cc',brand:'Keeway',type:'motorcycle',pricePerDay: 600},
            //sedan
            {noOfWheels:4,model:'Mirage G4',brand:'Mitsubishi',type:'sedan',pricePerDay: 1500},
            {noOfWheels:4,model:'Rio',brand:'Kia',type:'sedan',pricePerDay: 1500},
            {noOfWheels:4,model:'Accent',brand:'Hyundai',type:'sedan',pricePerDay: 1500},
            {noOfWheels:4,model:'Almera',brand:'Nissan',type:'sedan',pricePerDay: 1500},
            {noOfWheels:4,model:'Vios',brand:'Toyota',type:'sedan',pricePerDay: 1500},
            //SUV
            {noOfWheels:4,model:'RAV4',brand:'Toyota',type:'suv',pricePerDay: 2500},
            {noOfWheels:4,model:'Rogue',brand:'Nissan',type:'suv',pricePerDay: 2500},
            {noOfWheels:4,model:'Sorento',brand:'Kia',type:'suv',pricePerDay: 2500},
            {noOfWheels:4,model:'Explorer',brand:'Ford',type:'suv',pricePerDay: 2500},
            {noOfWheels:4,model:'CR-V',brand:'Honda',type:'suv',pricePerDay: 2500},
            //pickup
            {noOfWheels:4,model:'Colorado',brand:'Chevrolet',type:'pickup',pricePerDay: 3000},
            {noOfWheels:4,model:'Ranger',brand:'Ford',type:'pickup',pricePerDay: 3000},
            {noOfWheels:4,model:'D-Max',brand:'Isuzu',type:'pickup',pricePerDay: 3000},
            {noOfWheels:4,model:'BT-50',brand:'Mazda',type:'pickup',pricePerDay: 3000},
            {noOfWheels:4,model:'L-200',brand:'Mitsubishi',type:'pickup',pricePerDay: 3000},
            //van
            {noOfWheels:4,model:'Starex',brand:'Hyundai',type:'van',pricePerDay: 3200},
            {noOfWheels:4,model:'HiAce',brand:'Toyota',type:'van',pricePerDay: 3200},
            {noOfWheels:4,model:'UrVan',brand:'Nissan',type:'van',pricePerDay: 3200},
            {noOfWheels:4,model:'Carnival',brand:'Kia',type:'van',pricePerDay: 3200},
            {noOfWheels:4,model:'Innova',brand:'Toyota',type:'van',pricePerDay: 3200},
            //minibus
            {noOfWheels:4,model:'H350',brand:'Hyundai',type:'minibus',pricePerDay: 4500},
            {noOfWheels:4,model:'Coaster',brand:'Toyota',type:'minibus',pricePerDay: 4500},
            {noOfWheels:4,model:'L300',brand:'Mitsubishi',type:'minibus',pricePerDay: 4500},
            {noOfWheels:4,model:'Town',brand:'Chrysler',type:'minibus',pricePerDay: 4500},
            {noOfWheels:4,model:'Besta',brand:'Kia',type:'minibus',pricePerDay: 4500}
        ];
     }

     getAllVehicles(): Array<Vehicle>{
          return this.vehicleCollection;
     }
}
