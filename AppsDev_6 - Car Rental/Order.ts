let rentalChoices = document.getElementsByTagName("button");

let receiptVisibility = document.getElementById("receipt") as HTMLElement;


export class Order {
    constructor() {
        for (let i =0; i<rentalChoices.length; i++){
        rentalChoices[i].addEventListener("click", (e: Event) => this.buttonEvent(Number(rentalChoices[i].value)));
        console.log("RentalChoices" + rentalChoices[i].value);
        }
    }

    buttonEvent(n:number){
        //console.log(n);
        receiptVisibility.style.display="block";
        let selectedOption = document.getElementById("option_"+n);
        var receiptData=selectedOption.innerText.split("\n");
        //console.log(receiptData);
        let userNameInput = document.getElementById("userName") as HTMLInputElement;
        let numberInput = document.getElementById("phoneNumber") as HTMLInputElement;
        let daysInput = document.getElementById("days") as HTMLInputElement;
        document.getElementById("vehicle").innerHTML = receiptData[0];
        document.getElementById("user").innerHTML = userNameInput.value=="" ? "Name Unavailable" : userNameInput.value;
        document.getElementById("number").innerHTML = numberInput.value=="" ? "Number Unavailable" : numberInput.value;
        document.getElementById("cost").innerHTML = receiptData[2];
        document.getElementById("daysToRent").innerHTML = daysInput.value=="" ? "1" : daysInput.value;
        var costStr = receiptData[2].match(/(\d+)/);
        if (numberInput.value=="")
            document.getElementById("balance").innerHTML = 1 * Number(costStr[0]); 
        else 
            document.getElementById("balance").innerHTML = Number(daysInput.value) * Number(costStr[0]);
    }

    hideReceipt(){
        receiptVisibility.style.display="none";
    }
}