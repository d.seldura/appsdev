export interface Metahuman {
    callSign: string;
    firstName: string;
    lastName: string;
    group: string;
    metaHumanSkills?: Array<string>;
    metaHumanAbilities?: Array<string>;
    villain: boolean;
}