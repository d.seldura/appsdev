import { Heroes } from './Heroes';
import { Metahuman } from './Metahuman';

export class Avengers extends Heroes{
        constructor(heroCollection: Array<Metahuman>){
             super(heroCollection);      
        } 
        
        getAllAvengers(): Array<Metahuman>{
            let heroContainer: Array<Metahuman> = [];
            for(let index of this.heroCollection){
                if(index.group === "Avengers"){
                    heroContainer.push(index);
                }
            }
            return heroContainer;                  
        } 
}