import { Metahuman } from './Metahuman';

export class MetaFactory {
     private metaCollection: Array<Metahuman> = [];

     constructor(){
         this.metaCollection = [
           {callSign:'Iron Man',firstName:'Anthony',lastName:'Stark',group:'Avengers' , villain: false},
           {callSign:'Spider Man',firstName:'Peter',lastName:'Parker',group:'Avengers' , villain: false},
           {callSign:'Powerman',firstName:'Lucas',lastName:'Cage',group:'Heroes for Hire' , villain: false},
           {callSign:'Daredevil',firstName:'Matthew',lastName:'Murdock',group:'Heroes for Hire' , villain: false},   
           {callSign:'Iron Fist',firstName:'Daniel',lastName:'Rand',group:'Heroes for Hire' , villain: false},
           {callSign:'Doctor Doom',firstName:'Victor',lastName:'von Doom',group:'None' , villain: true},
           {callSign:'Titanium Man',firstName:'Anton',lastName:'Vanko',group:'None' , villain: true},   
           {callSign:'Thanos',firstName:'Thanos',lastName:'Unknown',group:'None' , villain: true},    
           {callSign:'Black Widow',firstName:'Natasha',lastName:'Romanova',group:'Avengers' , villain: false},                                                                      
         ];
     }

     getAllMetas(): Array<Metahuman>{
          return this.metaCollection;
     }
}
