import { Metahuman } from './Metahuman';

export class Heroes {
      protected heroCollection: Array<Metahuman> = [];

      constructor(metaCollection: Array<Metahuman>){
             this.heroCollection = metaCollection; 
      }

     getAllHeroes(): Array<Metahuman>{
           let heroContainer: Array<Metahuman> = [];
           for(let index of this.heroCollection){
               if(!index.villain){
                   heroContainer.push(index);
               }
           }
           return heroContainer;
     } 
}