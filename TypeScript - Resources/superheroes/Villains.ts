import { Metahuman } from './Metahuman';

export class Villains {
      private villainCollection: Array<Metahuman> = [];

      constructor(metaCollection: Array<Metahuman>){
             this.villainCollection = metaCollection; 
      }

     getAllVillains(): Array<Metahuman>{
           let villainContainer: Array<Metahuman> = [];
           for(let index of this.villainCollection){     
               if(index.villain){
                   villainContainer.push(index);
               }
           }
            
           return villainContainer;
     } 
}