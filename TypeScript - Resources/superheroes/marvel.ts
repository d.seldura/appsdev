import { Heroes } from './Heroes';
import { Villains } from './Villains';
import { Avengers } from './Avengers';
import { MetaFactory } from './MetaFactory';
import { Metahuman } from './Metahuman';

let metaData = new MetaFactory();

let heroes = new Heroes(metaData.getAllMetas());
let villains = new Villains(metaData.getAllMetas());
let avengers = new Avengers(metaData.getAllMetas());

console.log(metaData.getAllMetas());
console.log('\n\n');
console.log(heroes.getAllHeroes());
console.log('\n\n');
console.log(villains.getAllVillains());
console.log('\n\n');
console.log(avengers.getAllAvengers());