var btn = document.getElementsByTagName("button");
var num1data = document.getElementById("num1");
var num2data = document.getElementById("num2");
var resultdata = document.getElementById("result");
var len = btn.length;
var Calculator = /** @class */ (function () {
    function Calculator() {
        var _this = this;
        this.currentVal = 0;
        btn[0].addEventListener("click", function (e) { return _this.buttonEvent(0); });
        btn[1].addEventListener("click", function (e) { return _this.buttonEvent(1); });
        btn[2].addEventListener("click", function (e) { return _this.buttonEvent(2); });
        btn[3].addEventListener("click", function (e) { return _this.buttonEvent(3); });
        btn[4].addEventListener("click", function (e) { return _this.buttonEvent(4); });
        btn[5].addEventListener("click", function (e) { return _this.buttonEvent(5); });
        for (var i = 0; i < len; i++) {
            console.log(i + " :" + btn[i].value);
        }
    }
    /*
    0 :*
    1 :/
    2 :+
    3 :-
    4 :=
    */
    Calculator.prototype.buttonEvent = function (n) {
        console.log("button event" + n);
        this.num1 = Number(num1data.value);
        this.num2 = Number(num2data.value);
        switch (n) {
            //operands
            case 0: //multiply
                this.currentVal = this.num1;
                this.multiply(this.num2);
                console.log(this.currentVal);
                break;
            case 1: //divide
                this.currentVal = this.num1;
                this.divide(this.num2);
                console.log(this.currentVal);
                break;
            case 2: //add
                this.currentVal = this.num1;
                this.add(this.num2);
                console.log(this.currentVal);
                break;
            case 3: //subtract
                this.currentVal = this.num1;
                this.subtract(this.num2);
                console.log(this.currentVal);
                break;
            case 4:
                this.result();
                console.log(this.currentVal);
                break;
            case 5:
                resultdata.innerHTML = "";
                num1data.value = '';
                num2data.value = '';
                break;
        }
    };
    Calculator.prototype.result = function () {
        console.log(this.currentVal + "potato");
        resultdata.innerHTML = String(this.currentVal);
    };
    Calculator.prototype.multiply = function (operand) {
        this.currentVal *= operand;
    };
    Calculator.prototype.divide = function (operand) {
        if (operand == 0) {
            alert("Illegal division by zero");
            return;
        }
        else {
            this.currentVal /= operand;
        }
    };
    Calculator.prototype.add = function (operand) {
        this.currentVal += operand;
    };
    Calculator.prototype.subtract = function (operand) {
        this.currentVal -= operand;
    };
    return Calculator;
}());
// start the app
var calcu1337or = new Calculator();
