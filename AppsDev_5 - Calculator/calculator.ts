var btn = document.getElementsByTagName("button");
var num1data = document.getElementById("num1") as HTMLInputElement;
var num2data = document.getElementById("num2") as HTMLInputElement;
var resultdata = document.getElementById("result");
var len = btn.length;

class Calculator {
    num1: any;
    num2: any;
    currentVal: number;

    constructor() {
        this.currentVal = 0;
        btn[0].addEventListener("click", (e: Event) => this.buttonEvent(0));
        btn[1].addEventListener("click", (e: Event) => this.buttonEvent(1));
        btn[2].addEventListener("click", (e: Event) => this.buttonEvent(2));
        btn[3].addEventListener("click", (e: Event) => this.buttonEvent(3));
        btn[4].addEventListener("click", (e: Event) => this.buttonEvent(4));
        btn[5].addEventListener("click", (e: Event) => this.buttonEvent(5));
        for (var i = 0; i < len; i++) {
            console.log(i + " :" + btn[i].value);
        }
    }
    /*
    0 :*
    1 :/
    2 :+
    3 :-
    4 :=
    */
    buttonEvent(n: number) {
        console.log("button event" + n)
        this.num1 = Number(num1data.value);
        this.num2 = Number(num2data.value);
        switch (n) {
            //operands
            case 0: //multiply
                this.currentVal = this.num1;
                this.multiply(this.num2);
                console.log(this.currentVal);
                break;
            case 1: //divide
                this.currentVal = this.num1;
                this.divide(this.num2);
                console.log(this.currentVal);
                break;
            case 2: //add
                this.currentVal = this.num1;
                this.add(this.num2);
                console.log(this.currentVal);
                break;
            case 3: //subtract
                this.currentVal = this.num1;
                this.subtract(this.num2);
                console.log(this.currentVal);
                break;
            case 4:
                this.result();
                console.log(this.currentVal);
                break;
            case 5:
                resultdata.innerHTML = "";
                num1data.value='';
                num2data.value='';
                break;
        }
    }
    result(): void {
        console.log(this.currentVal+"potato");
        resultdata.innerHTML = String(this.currentVal)
    }

    multiply(operand: number) {
        this.currentVal *= operand
    }
    divide(operand: number) {
        if (operand == 0) {
            alert("Illegal division by zero");
            return;
        } else {
            this.currentVal /= operand
        }
    }
    add(operand: number) {
        this.currentVal += operand
    }

    subtract(operand: number) {
        this.currentVal -= operand
    }
}
// start the app
var calcu1337or = new Calculator();