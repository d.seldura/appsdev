var recipe = [
	["Onions",5],
	["Garlic",5],
	["Cheese",10],
	["Tomatoes",5],
	["Lettuce",5],
	["Brioche",3],
	["Egg",10],
	["Ham",15],
	["Burger Patty",15],
	["Bacon",20]
];

function generateForm(){
	var generated="<table>";
	for (var i=0;i<recipe.length;i++){
	generated = generated + "<tr><td><input type=\"checkbox\" id=\"myCheck" +i+"\"></td><td id=\"name"+i+"\">"+ recipe[i][0] +"</td><td id=\"value"+i+"\">"+recipe[i][1]+"</td></tr>"
	}
	generated = generated + "</table>";
	document.getElementById("burgerOptions").innerHTML = generated;
}

function create(){
	var burgerSelections = document.getElementsByTagName("input");
	var total=0;
	var burgerOrder = "<table>";
	burgerOrder = burgerOrder + "<tr><td>" + document.getElementById("username").value + "'s Order" + "</td></tr>";
	for(var i=0;i<recipe.length;i++){
		if (document.getElementById("myCheck"+i).checked){
			total = total + recipe[i][1];
			burgerOrder = burgerOrder + "<tr><td>" + recipe[i][0] +"</td><td>"+ recipe[i][1] + "</td></tr>"
		}
	}
	burgerOrder = burgerOrder + "<tr></tr><tr><td>Total</td>" + "<td>Php." + total + "</td></tr>"
	burgerOrder = burgerOrder + "</table>";
	document.getElementById("burgerOrder").innerHTML = burgerOrder;
}
