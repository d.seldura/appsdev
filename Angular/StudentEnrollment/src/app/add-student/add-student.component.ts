import { Component, OnInit } from '@angular/core';
import { DbFireStoreService } from 'src/app/db-fire-store.service';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {
  constructor(private FSDBHandler: DbFireStoreService) {}

  ngOnInit() {}


  getData() {
    const formData = document.getElementsByTagName('input');
    for (let x = 0; x < 4; x++) {
      if (formData[x].value === '' || (!formData[4].checked && !formData[5].checked)) {
        console.log(formData[x].value, !formData[4].checked, !formData[5].checked);
        alert('No fields must be left blank');
        return;
      }
    }
    const student = {
      firstName: formData[0].value,
      lastName: formData[1].value,
      ID: formData[2].value,
      dateOfBirth: formData[3].value,
      gender: '',
      image: formData[6].value
    };

    for (let i = 4; i < 6; i++) {
      if (formData[i].checked) {
        student.gender = formData[i].value;
        break;
      }
    }
    if (student.image === '') {
      student.image = 'https://via.placeholder.com/150.png?text=STUDENT+IMAGE';
    }
    this.FSDBHandler.sendData(student, 'studentDB');
    alert('Data Added');
    const resetForm = document.getElementById('myForm') as HTMLFormElement;
    resetForm.reset();
  }
}
