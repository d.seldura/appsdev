import { TestBed } from '@angular/core/testing';

import { DbFireStoreService } from './db-fire-store.service';

describe('DbFireStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DbFireStoreService = TestBed.get(DbFireStoreService);
    expect(service).toBeTruthy();
  });
});
