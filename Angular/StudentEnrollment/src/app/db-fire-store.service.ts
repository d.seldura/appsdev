import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class DbFireStoreService {

  constructor(private firestore: AngularFirestore) { }

sendData(data: any, collection: string) {
  return new Promise<any>((resolve, reject) =>{
      this.firestore
          .collection(collection)
          .add(data)
          .then(res => {}, err => reject(err));
  });
}

retrieveData(collection: string) {
  return this.firestore.collection(collection).snapshotChanges();
}

}
