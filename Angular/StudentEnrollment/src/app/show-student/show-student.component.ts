import { Component, OnInit } from '@angular/core';
import { DbFireStoreService } from 'src/app/db-fire-store.service';

@Component({
  selector: 'app-show-student',
  templateUrl: './show-student.component.html',
  styleUrls: ['./show-student.component.css']
})
export class ShowStudentComponent implements OnInit {
  private currentData;
  constructor(private FSDBHandler: DbFireStoreService) {}

  ngOnInit() {
    this.FSDBHandler.retrieveData('studentDB').subscribe(res => (this.currentData = res));
  }
}
