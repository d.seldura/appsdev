// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDoS2HL_AJJ6lIy6O7ABLAXjUffufGn8PI',
    authDomain: 'appsdev-student-enrollment.firebaseapp.com',
    databaseURL: 'https://appsdev-student-enrollment.firebaseio.com',
    projectId: 'appsdev-student-enrollment',
    storageBucket: 'appsdev-student-enrollment.appspot.com',
    messagingSenderId: '587498768965',
    appId: '1:587498768965:web:ea971e0e445396bf94b12c'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
