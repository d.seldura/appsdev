function Loanee(fName, lName, loanAmount, pDuration) {
    this.recipient = fName + " " + lName;
    this.loanAmount = loanAmount;
    this.pDuration = pDuration;
}

Loanee.getRelevantValues = function() {
    if (this.loanAmount < LoanCategories.category0)
        return [0, 0];
    else if (this.loanAmount <= LoanCategories.category1)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category0), SurchargeCategories.category0];
    else if (this.loanAmount < LoanCategories.category2)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category1), SurchargeCategories.category1];
    else if (this.loanAmount < LoanCategories.category3)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category2), SurchargeCategories.category2];
    else if (this.loanAmount < LoanCategories.category4)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category3), SurchargeCategories.category3];
    else if (this.loanAmount < LoanCategories.category5)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category4), SurchargeCategories.category4];
    else if (this.loanAmount < LoanCategories.category6)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category5), SurchargeCategories.category5];
    else if (this.loanAmount < LoanCategories.category8)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category6), SurchargeCategories.category6];
    else if (this.loanAmount < LoanCategories.category9)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category7), SurchargeCategories.category7];
    else if (this.loanAmount < LoanCategories.category10)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category8), SurchargeCategories.category8];
    else if (this.loanAmount < LoanCategories.category11)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category9), SurchargeCategories.category9];
    else if (this.loanAmount < LoanCategories.category12)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category10), SurchargeCategories.category10];
    else if (this.loanAmount < LoanCategories.category13)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category11), SurchargeCategories.category11];
    else if (this.loanAmount < LoanCategories.category15)
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category12), SurchargeCategories.category12];
    else
        return [parseFloat(this.loanAmount) + parseFloat(this.loanAmount * SurchargeCategories.category13), SurchargeCategories.category13];
}


var loanForm = document.getElementById('entry-form');
var submitBtn = document.getElementById('submit-form');
var resetBtn = document.getElementById('reset-form');
var amortizationTable = document.getElementById('amortization-table');
var loanAmount = document.getElementById('loan-amount');

var TermCategories = {
    category0: 6,
    category1: 12,
    category2: 24,
    category3: 36
};

var LoanCategories = {
    category0: 1000,
    category1: 2000,
    category2: 5000,
    category3: 7000,
    category4: 9000,
    category5: 11000,
    category6: 15000,
    category7: 20000,
    category8: 25000,
    category9: 35000,
    category10: 45000,
    category11: 55000,
    category12: 65000,
    category13: 75000,
    category14: 90000,
    category15: 100000,
    category16: 200000
};

var SurchargeCategories = {
    category0: 0,
    category1: 0.0325,
    category2: 0.04,
    category3: 0.045,
    category4: 0.05,
    category5: 0.055,
    category6: 0.06,
    category7: 0.065,
    category8: 0.07,
    category9: 0.075,
    category10: 0.08,
    category11: 0.085,
    category12: 0.09,
    category13: 0.10
};


function displayTermsBasedOnAmount() {
    var loanAmount = document.getElementById("loan-amount").value
    if (loanAmount < LoanCategories.category0) {
        optionItem = document.createElement('option')
        optionItem.cloneNode(true);
        optionItem.innerHTML = ("Value below allowable range!")
        document.getElementById('payment-duration').appendChild(optionItem);
    } else if (loanAmount < LoanCategories.category2)
        displayTermsMenu(TermCategories.category0, document.getElementById('payment-duration'));
    else if (loanAmount < LoanCategories.category14)
        displayTermsMenu(TermCategories.category1, document.getElementById('payment-duration'));
    else if (loanAmount < LoanCategories.category16)
        displayTermsMenu(TermCategories.category2, document.getElementById('payment-duration'));
    else
        displayTermsMenu(TermCategories.category3, document.getElementById('payment-duration'));
}

function displayTermsMenu(terms, element) {
    var newOption = document.createElement('option');
    var counter;
    var optionItem = newOption.cloneNode(true);
    var newOptionItem = element.appendChild(optionItem);
    element.innerHTML = ""; //ensure it doesn't double up
    for (counter = 1; counter <= terms; counter++) {
        optionItem = newOption.cloneNode(true);
        optionItem.setAttribute('value', counter);
        newOptionItem = element.appendChild(optionItem);

        if (counter == 6) { //sets the default option of the select menu
            newOptionItem.selected = true;
        }

        if (counter == 1)
            newOptionItem.innerHTML = "Payment of balance in " + counter + " month";
        else
            newOptionItem.innerHTML = "Payment of balance in " + counter + " months";
    }
}

function computeLoanDetails() {
    fName = document.getElementById("fname").value
    lName = document.getElementById("lname").value
    loanAmount = document.getElementById("loan-amount").value
    pDuration = document.getElementById("payment-duration").value
    var client = new Loanee(fName, lName, loanAmount, pDuration);
    var loanData = Loanee.getRelevantValues.call(client);
    document.getElementById("loan-recepient").innerHTML = client.recipient;
    document.getElementById("loan-principal").innerHTML = client.loanAmount;
    document.getElementById("loan-surcharge").innerHTML = loanData[1];
    document.getElementById("loan-terms").innerHTML = client.pDuration;
    document.getElementById("total-loan-amount").innerHTML = loanData[0];

    var payment = loanData[0] / client.pDuration;
    var balance = loanData[0];
    for (i = 1; i <= client.pDuration; i++) {
        balance -= payment;
        document.getElementById("amortization-table").innerHTML += "<tr><td>" + i + "</td><td>" + payment.toFixed(2) + "</td><td>" + balance.toFixed(2) + "</td>";
    }

}